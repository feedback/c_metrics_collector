package metrics.FunctionMetrics;

import lombok.Data;
import metrics.Locatable;

/**
 * Contains function-level data acquired from metrics.json
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/Data
 *
 * @author Nathan Laundry
 * @since 2020-05-15
 */

@Data
public class Parameter implements Locatable {
    private String name;
    private String type;
    private boolean isPtr;
    
    @Override
    public String getLocation() {
        return "stub location";
    }
}
