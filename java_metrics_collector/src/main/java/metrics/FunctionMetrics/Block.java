package metrics.FunctionMetrics;

import lombok.Data;
import metrics.Locatable;

/**
 * Contains function-level data acquired from metrics.json
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/Data
 *
 * @author Nathan Laundry
 * @since 2020-05-15
 */

@Data
public class Block implements Locatable {
    private String type;
    private String location;
    
    @Override
    public String getLocation() {
        return "stub location";
    }
}
