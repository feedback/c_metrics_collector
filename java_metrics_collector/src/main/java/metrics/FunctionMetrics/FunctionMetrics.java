package metrics.FunctionMetrics;

import lombok.Data;
import metrics.Locatable;

/**
 * Contains function-level data acquired from metrics.json
 *
 * Lombok annotations used: https://projectlombok.org/features/Data
 *
 * @author Nathan Laundry
 * @since 2020-05-15 a
 */
@Data
public class FunctionMetrics implements Locatable {
    private String functionName;
    private String coord;
    // Capitalized to match the output of metrics.json DO NOT CHANGE
    private ParameterListData ParameterListData;
    private NumberOfBlocksInFunction NumberOfBlocksInFunction;

    @Override
    public String getLocation() {
        return coord;
    }
}
