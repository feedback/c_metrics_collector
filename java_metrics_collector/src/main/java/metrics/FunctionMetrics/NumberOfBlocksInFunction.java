package metrics.FunctionMetrics;

import lombok.Data;
import metrics.Locatable;
import java.util.ArrayList;

/**
 * Contains function-level data acquired from metrics.json
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/Data
 *
 * @author Nathan Laundry
 * @since 2020-05-15
 */

@Data
public class NumberOfBlocksInFunction implements Locatable {
    private int NumberOfBlocksInFunction;
    private ArrayList<Block> blocks;
    
    @Override
    public String getLocation() {
        return "stub location";
    }

}
