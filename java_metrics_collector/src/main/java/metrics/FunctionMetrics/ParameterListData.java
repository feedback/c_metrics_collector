package metrics.FunctionMetrics;

import lombok.Data;
import java.util.ArrayList;
import metrics.Locatable;

/**
 * Contains function-level data acquired from metrics.json
 *
 * Lombok annotations used: https://projectlombok.org/features/Data
 *
 * @author Nathan Laundry
 * @since 2020-05-15
 */

@Data
public class ParameterListData implements Locatable {
    private int numParams;
    private ArrayList<Parameter> paramList;

    @Override
    public String getLocation() {
        return "stub location";
    }
}
