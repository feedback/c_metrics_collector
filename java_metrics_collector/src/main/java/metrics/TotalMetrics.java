package metrics;

import lombok.Data;
import metrics.FunctionMetrics.*;

import java.util.ArrayList;

/**
 * Contains ArrayLists of all {@link ClassMetrics ClassMetrics}, {@link FieldMetrics FieldMetrics},
 * {@link MethodMetrics MethodMetrics}, and {@link VariableMetrics VariableMetrics} objects acquired from the parsed
 * csv files. This is the primary source of code metrics for all {@link sniffer.Sniffer Sniffers}.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/Data
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Data
public class TotalMetrics {

    private final ArrayList<FunctionMetrics> functionMetrics;

    public boolean isEmpty() {
        return functionMetrics.size() == 0;
    }
}
