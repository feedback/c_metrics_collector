package analyzer;

import lombok.Getter;
import smell.Smell;
import smell.SmellCount;

import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper for all analysis done by the program.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/GetterSetter
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Getter
public class Analysis {

    private String title;
    private List<Smell> feedbackItems;
    private List<SmellCount> overview;

    public Analysis(List<Smell> feedbackItems, String title) {
        setTitle(title);
        setFeedbackItems(feedbackItems);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFeedbackItems(List<Smell> feedbackItems) {
        this.feedbackItems = feedbackItems;
        setOverview();
    }

    private void setOverview() {
        this.overview = new ArrayList<>();
        for (Smell smell : feedbackItems) {
            overview.add(new SmellCount(smell.getName(), smell.getCount()));
        }
    }
}
