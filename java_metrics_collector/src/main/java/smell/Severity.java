package smell;

/**
 * Severity level of a Smell.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
public enum Severity {

    /**
     * No smell found.
     */
    NONE("NONE"),

    /**
     * Minor smell. May even be false positive.
     */
    MINOR("MINOR"),

    /**
     * Moderate smell. Almost certainly exists and is noticeable.
     */
    MODERATE("MODERATE"),

    /**
     * Major smell. Very noticeable and should definitely be fixed.
     */
    MAJOR("MAJOR");

    private String severity;

    Severity(String severity) {
        this.severity = severity;
    }

    /**
     * Returns the string representation of the severity level.
     *
     * @return one of the following strings: "NONE", "MINOR", "MODERATE", "MAJOR"
     */
    @Override
    public String toString() {
        return severity;
    }
}
