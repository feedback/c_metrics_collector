package smell;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Container for the name and frequency of a smell. Used for creating the "Overview" portion of smell.json.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/GetterSetter
 * https://projectlombok.org/features/constructor
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Getter
@Setter
@AllArgsConstructor
public class SmellCount {

    private String name;
    private int count;

    public SmellCount() {
        this("", 0);
    }
}
