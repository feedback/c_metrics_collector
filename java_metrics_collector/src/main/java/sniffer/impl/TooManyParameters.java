package sniffer.impl;

import metrics.FunctionMetrics.FunctionMetrics;
import metrics.TotalMetrics;
import smell.Severity;
import smell.Smell;
import sniffer.SettableSniffer;

public class TooManyParameters extends SettableSniffer {

    public TooManyParameters(String className, String recommendation, String reasoning, int minorThreshold,
            int moderateThreshold, int majorThreshold) {

        super(className, recommendation, reasoning, minorThreshold, moderateThreshold, majorThreshold);
    }

    @Override
    public Smell sniff(TotalMetrics totalMetrics) {
        Smell smell = new Smell("Too Many Parameters", getRecommendation(), getReasoning());

        for (FunctionMetrics functionMetrics : totalMetrics.getFunctionMetrics()) {
            String location = functionMetrics.getLocation();
            Severity severity = calculateSeverityAscending(functionMetrics.getParameterListData().getNumParams());
            smell.addLocation(location, severity);
        }

        return smell;
    }
}
