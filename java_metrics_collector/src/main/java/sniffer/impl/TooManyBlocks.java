package sniffer.impl;

import metrics.FunctionMetrics.FunctionMetrics;
import metrics.TotalMetrics;
import smell.Severity;
import smell.Smell;
import sniffer.SettableSniffer;

public class TooManyBlocks extends SettableSniffer {

    public TooManyBlocks(String className, String recommendation, String reasoning, int minorThreshold,
            int moderateThreshold, int majorThreshold) {

        super(className, recommendation, reasoning, minorThreshold, moderateThreshold, majorThreshold);
    }

    @Override
    public Smell sniff(TotalMetrics totalMetrics) {
        Smell smell = new Smell("Too Many Blocks", getRecommendation(), getReasoning());

        for (FunctionMetrics functionMetrics : totalMetrics.getFunctionMetrics()) {
            String location = functionMetrics.getLocation();
            Severity severity = calculateSeverityAscending(
                    functionMetrics.getNumberOfBlocksInFunction().getNumberOfBlocksInFunction());
            smell.addLocation(location, severity);
        }

        return smell;
    }
}
