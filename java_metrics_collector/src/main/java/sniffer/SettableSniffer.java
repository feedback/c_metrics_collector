package sniffer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import smell.Severity;

/**
 * Abstraction of a {@link sniffer.Sniffer Sniffer} that contains additional information about the produced
 * {@link smell.Smell Smell}, as well as severity threshold configuration.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/GetterSetter
 * https://projectlombok.org/features/constructor
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Getter
@AllArgsConstructor
public abstract class SettableSniffer implements Sniffer {

    private final String className;
    private final String recommendation;
    private final String reasoning;
    private final int minorThreshold;
    private final int moderateThreshold;
    private final int majorThreshold;

    protected Severity calculateSeverityAscending(int value) {
        if (value >= majorThreshold)
            return Severity.MAJOR;
        if (value >= moderateThreshold)
            return Severity.MODERATE;
        if (value >= minorThreshold)
            return Severity.MINOR;
        return Severity.NONE;
    }

    protected Severity calculateSeverityDescending(int value) {
        if (value <= majorThreshold)
            return Severity.MAJOR;
        if (value <= moderateThreshold)
            return Severity.MODERATE;
        if (value <= minorThreshold)
            return Severity.MINOR;
        return Severity.NONE;
    }
}
