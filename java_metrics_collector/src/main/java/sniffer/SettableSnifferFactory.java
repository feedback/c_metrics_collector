package sniffer;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used to initialize all {@link sniffer.SettableSniffer SettableSniffers}. Initialization is done
 * by reading /resources/settings.yaml for the settings of each {@link sniffer.SettableSniffer SettableSniffer}. Each
 * {@link sniffer.Sniffer Sniffer} with the property "used: true" is instantiated via reflection.
 *
 * @author Shawn Kaplan
 * @since 2020-05-16
 */
public final class SettableSnifferFactory {

    private SettableSnifferFactory() {}

    /**
     * For each {@link sniffer.SnifferSetting SettableSniffer} found in /resources/settings.yaml, adds a newly
     * initialized {@link sniffer.SettableSniffer SettableSniffer} object to a list which is returned.
     * Instantiation is done via reflection.
     *
     * @see sniffer.Sniffer
     */
    public static List<Sniffer> createSniffers(InputStream inputStream) {
        ArrayList<Sniffer> sniffers = new ArrayList<>();
        for (SnifferSetting setting : getSnifferSettings(inputStream)) {
            if (setting.isUsed()) {
                SettableSniffer sniffer = createSettableSniffer(setting);
                if (sniffer != null) {
                    sniffers.add(sniffer);
                }
            }
        }
        return sniffers;
    }

    private static List<SnifferSetting> getSnifferSettings(InputStream inputStream) {
        Yaml yaml = new Yaml(new Constructor(SnifferSetting.class));
        ArrayList<SnifferSetting> settings = new ArrayList<>();
        for (Object object : yaml.loadAll(inputStream)) {
            settings.add((SnifferSetting) object);
        }
        return settings;
    }

    private static SettableSniffer createSettableSniffer(SnifferSetting setting) {
        try {
            Class<? extends SettableSniffer> sniffer = Class.forName("sniffer.impl." + setting.getClassName())
                    .asSubclass(SettableSniffer.class);
            return sniffer
                    .getConstructor(String.class, String.class, String.class, int.class, int.class, int.class)
                    .newInstance(
                            setting.getClassName(),
                            setting.getRecommendation(),
                            setting.getReasoning(),
                            setting.getMinorThreshold(),
                            setting.getModerateThreshold(),
                            setting.getMajorThreshold());
        } catch (ClassNotFoundException e) {
            System.err.println("Could not find class: " + setting.getClassName());
        } catch (NoSuchMethodException e) {
            System.err.println("Could not find constructor for class: " + setting.getClassName());
        } catch (Exception e) {
            System.err.println("Could not instantiate class: " + setting.getClassName());
        }
        return null;
    }
}
