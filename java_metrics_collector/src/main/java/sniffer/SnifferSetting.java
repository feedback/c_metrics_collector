package sniffer;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Defines the settings for a new {@link sniffer.Sniffer Sniffer} object.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/Data
 * https://projectlombok.org/features/constructor
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Data
@NoArgsConstructor
public class SnifferSetting {

    private boolean used;
    private String className;
    private String recommendation;
    private String reasoning;
    private int minorThreshold;
    private int moderateThreshold;
    private int majorThreshold;
}
