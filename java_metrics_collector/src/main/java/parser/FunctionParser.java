package parser;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.Gson;

// import jdk.nashorn.internal.objects.annotations.Function;

import java.nio.file.Files;
import java.nio.file.Paths;

import metrics.FunctionMetrics.FunctionMetrics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Parses the json file containing function data, then returns an ArrayList of
 * {@link FunctionMetrics FunctionMetrics}. The expected file is part of the
 * output from c_metrics_generator and is called metrics.json.
 *
 * @author Nathan Laundry
 * @since 2020-05-15
 */
public class FunctionParser implements Parser<FunctionMetrics> {

    @Override
    public ArrayList<FunctionMetrics> parse(String filepath) throws Exception {
        ArrayList<FunctionMetrics> list = new ArrayList<FunctionMetrics>();

        String fileData = new String(Files.readAllBytes(Paths.get(filepath)));
        list = new ArrayList<FunctionMetrics>(
                Arrays.asList(new GsonBuilder().create().fromJson(fileData, FunctionMetrics[].class)));
        return list;
    }
}
