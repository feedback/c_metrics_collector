=======
Credits
=======

Development Lead
----------------

* Nathan Laundry <nlaundry@uoguelph.ca>

Contributors
------------

* Shawn Kaplan: for contributing integral structural design ideas 
