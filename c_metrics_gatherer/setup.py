#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [ 'pycparser' ]

setup_requirements = [ ]

test_requirements = [ ]

setup(
    author="Nathan Laundry",
    author_email='nlaundry@uoguelph.ca',
    python_requires='>=3.5',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Gathers metrics from source C files by generating an AST from pycparser",
    entry_points={
        'console_scripts': [
            'c_metrics_gatherer=c_metrics_gatherer.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='c_metrics_gatherer',
    name='c_metrics_gatherer',
    packages=find_packages(include=['c_metrics_gatherer', 'c_metrics_gatherer.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/nlaundry/c_metrics_gatherer',
    version='0.1.0',
    zip_safe=False,
)
