"""Main module."""
import json
import os
import sys

from .generate_ast import file_to_dict
from .run_gatherers import generate_metrics_json


def get_all_source_files(submission_directory):
    """ returns a list of the names of .c or .h files in the submission """
    project_files = [
        os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(submission_directory)
        for f in files
        if (f.endswith(".c")) or (f.endswith(".h"))
    ]
    return project_files


def generate_metrics(args):
    """ outputs the result of metrics gatherers to the output file """

    if len(sys.argv) > 1:
        source_files = get_all_source_files(args.submission)
        # creates a dictionary represenation of the AST for each source file in the project
        ast_dicts = [
            file_to_dict(sourceFile, args.c_headers) for sourceFile in source_files
        ]
        if ast_dicts == []:
            print("No valid source files given")
            return

        metrics_json = generate_metrics_json(ast_dicts)

        if metrics_json == []:
            print("no measurable metrics were produced")
            return

        with open(args.output, "w") as outfile:
            json.dump(metrics_json, outfile, indent=4)

    else:
        print("Please provide a filename as argument")
