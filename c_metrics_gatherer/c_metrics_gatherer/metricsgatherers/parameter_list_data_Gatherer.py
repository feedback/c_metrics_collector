""" ParameterListData returns the number of parameters and their types + names """

METRIC_NAME = "ParameterListData"


def gather_metric(function_node):
    """ mandatory gather metric function """
    # if there are no params return early with 0 and an empty paramlist
    if function_node.get("decl").get("type").get("args") is None:
        return METRIC_NAME, {"numParams": 0, "paramList": []}

    param_node_list = function_node.get("decl").get("type").get("args").get("params")
    param_json_list = [create_param_json(node) for node in param_node_list]

    return (
        METRIC_NAME,
        {"numParams": len(param_node_list), "paramList": param_json_list},
    )


def create_param_json(param_node):
    """ returns param data as a dictionary """
    param_types_list = param_node.get("type").get("type").get("names")
    param_type = param_types_list[0] if param_types_list is not None else "void"

    return {
        "name": param_node.get("name"),
        "type": param_type,
        "isPtr": param_node.get("type").get("_nodetype") == "PtrDecl",
    }
