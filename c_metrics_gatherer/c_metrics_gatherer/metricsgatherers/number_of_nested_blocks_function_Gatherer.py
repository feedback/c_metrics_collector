"""NumberOfNestedBlocksGatherer returns the total number of nested blocks in a function"""

METRIC_NAME = "NumberOfNestedBlocksInFunction"
COMPOUND_BLOCK_NODE_TYPES = ["For", "If", "While", "DoWhile", "Switch"]


def traverseIfTrue(blockList, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS):
    # print("If true\n")    

    if(blockList == None):
        return ongoingCount

    for block in blockList:
        if(block.get("_nodetype") == "If"):
            ongoingCount = ongoingCount + 1
            if(block.get("iftrue") != None):                
                ongoingCount = traverseIfTrue(block.get("iftrue").get("block_items"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)                
                BLOCK_TYPE_COUNTS["If"] = BLOCK_TYPE_COUNTS["If"] + 1
                NESTING_COORDS.append(block.get("coord"))

            if(block.get("iffalse") != None):
                ongoingCount = traverseIfFalse(block.get("iffalse"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)    

        elif(block.get("_nodetype") == "Switch"):
            # print("nested switch found\n")
            ongoingCount = ongoingCount + 1
            BLOCK_TYPE_COUNTS["Switch"] = BLOCK_TYPE_COUNTS["Switch"] + 1
            NESTING_COORDS.append(block.get("coord"))
            for embeddedCaseBlock in block.get("stmt").get("block_items"):
                caseBlockContents = embeddedCaseBlock.get("stmts")
                ongoingCount = traverseSwitch(caseBlockContents, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
        
        elif(block.get("_nodetype") == "For"):
            ongoingCount = ongoingCount + 1
            # print("Outer for found!\n")   
            forBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseFor(forBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)                
            BLOCK_TYPE_COUNTS["For"] = BLOCK_TYPE_COUNTS["For"] + 1

        elif(block.get("_nodetype") == "While"):
            ongoingCount = ongoingCount + 1
            # print("embedded While found!\n")
            whileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseWhile(whileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["While"] = BLOCK_TYPE_COUNTS["While"] + 1

        elif(block.get("_nodetype") == "DoWhile"):
            ongoingCount = ongoingCount + 1
            # print("inner DoWhile found!\n")
            doWhileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseDoWhile(doWhileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)    
            BLOCK_TYPE_COUNTS["DoWhile"] = BLOCK_TYPE_COUNTS["DoWhile"] + 1

    return ongoingCount


def traverseIfFalse(elseBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS):
    # print("If false\n")
    # print(elseBlock, end="\n\n\n")

    elseIfCheck = True

    if(elseBlock.get("_nodetype") == "Compound"):
        elseIfCheck = False         #Only an else block after the if
    elif(elseBlock.get("_nodetype") == "If"):
        elseIfCheck = True          #elseIf block after the if

    if(elseIfCheck == True):

        BLOCK_TYPE_COUNTS["If"] = BLOCK_TYPE_COUNTS["If"] - 1

        if(elseBlock.get("iftrue") != None):
            
            ongoingCount = traverseIfTrue(elseBlock.get("iftrue").get("block_items"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["If"] = BLOCK_TYPE_COUNTS["If"] + 1
            # NESTING_COORDS.append(elseBlock.get("coord"))

        if(elseBlock.get("iffalse") != None):

            ongoingCount = traverseIfFalse(elseBlock.get("iffalse"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)            
            # print(type(ongoingCount))
            return ongoingCount

            # if(elseBlock.get("iffalse").get("_nodetype") == "Compound"):
            #     blockList = elseBlock.get("iffalse").get("block_times")    
            # else:
            #     print(elseBlock, end="\nPOGGERS\n\n")
            #     return

            # print("\n\nI returned\n\n")

            # if( type(blockList) != "list"):
            #     print("\nHEREEEE\n")
            #     return
            
            # else:

            # or list( elseBlock.get("iffalse"))
            # print(blockList)            
            # print(elseBlock.get("iffalse"), end="\nXXXX\n")   # WHAT IF THE IFFALSE DOESN'T HAVE A COMPOUNT STATEMENT AND IS
            # IT IS JUST A FUNCITON AFTER AN IF WITHOUT BRACKETS 
            
            # NOT handling else if blocks correctly

            # print(blockList)            
            for block in blockList:
                if(block.get("_nodetype") == "If"):
                    ongoingCount = ongoingCount + 1
                    if(block.get("iftrue") != None):
                        ongoingCount = traverseIfTrue(block.get("iftrue").get("block_items"), ongoingCount)

                    if(block.get("iffalse") != None):
                        ongoingCount = traverseIfFalse(block.get("iffalse"), ongoingCount)    

                elif(block.get("_nodetype") == "Switch"):
                    # print("nested switch found\n")
                    ongoingCount = ongoingCount + 1
                    for embeddedCaseBlock in block.get("stmt").get("block_items"):
                        caseBlockContents = embeddedCaseBlock.get("stmts")
                        ongoingCount = traverseSwitch(caseBlockContents, ongoingCount)

                elif(block.get("_nodetype") == "For"):
                    ongoingCount = ongoingCount + 1
                    # print("Outer for found!\n")   
                    forBlock = block.get("stmt")
                    ongoingCount = traverseFor(forBlock, ongoingCount)    

                elif(block.get("_nodetype") == "While"):
                    ongoingCount = ongoingCount + 1
                    # print("embedded While found!\n")
                    whileBlock = block.get("stmt")
                    ongoingCount = traverseWhile(whileBlock, ongoingCount)

                elif(block.get("_nodetype") == "DoWhile"):
                    ongoingCount = ongoingCount + 1
                    # print("inner DoWhile found!\n")
                    doWhileBlock = block.get("stmt")
                    ongoingCount = traverseDoWhile(doWhileBlock, ongoingCount)    

    elif(elseIfCheck == False):
        
        if(elseBlock.get("_nodetype") == "Compound"):
            blockList = elseBlock.get("block_items")
        else:
            blockList = [elseBlock]
        
        if(blockList == None):
            return ongoingCount

        # print(elseBlock)

        for block in blockList:
            if(block.get("_nodetype") == "If"):
                ongoingCount = ongoingCount + 1
                if(block.get("iftrue") != None):
                    NESTING_COORDS.append(block.get("coord"))
                    ongoingCount = traverseIfTrue(block.get("iftrue").get("block_items"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
                    BLOCK_TYPE_COUNTS["If"] = BLOCK_TYPE_COUNTS["If"] + 1

                if(block.get("iffalse") != None):
                    ongoingCount = traverseIfFalse(block.get("iffalse"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)    

            elif(block.get("_nodetype") == "Switch"):
                # print("nested switch found\n")
                ongoingCount = ongoingCount + 1
                NESTING_COORDS.append(block.get("coord"))
                BLOCK_TYPE_COUNTS["Switch"] = BLOCK_TYPE_COUNTS["Switch"] + 1
                for embeddedCaseBlock in block.get("stmt").get("block_items"):
                    caseBlockContents = embeddedCaseBlock.get("stmts")
                    ongoingCount = traverseSwitch(caseBlockContents, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)

            elif(block.get("_nodetype") == "For"):
                ongoingCount = ongoingCount + 1
                # print("Outer for found!\n")   
                forBlock = block.get("stmt")
                NESTING_COORDS.append(block.get("coord"))
                ongoingCount = traverseFor(forBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)    
                BLOCK_TYPE_COUNTS["For"] = BLOCK_TYPE_COUNTS["For"] + 1

            elif(block.get("_nodetype") == "While"):
                ongoingCount = ongoingCount + 1
                # print("embedded While found!\n")
                whileBlock = block.get("stmt")
                NESTING_COORDS.append(block.get("coord"))
                ongoingCount = traverseWhile(whileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
                BLOCK_TYPE_COUNTS["While"] = BLOCK_TYPE_COUNTS["While"] + 1

            elif(block.get("_nodetype") == "DoWhile"):
                ongoingCount = ongoingCount + 1
                # print("inner DoWhile found!\n")
                doWhileBlock = block.get("stmt")
                NESTING_COORDS.append(block.get("coord"))
                ongoingCount = traverseDoWhile(doWhileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)    
                BLOCK_TYPE_COUNTS["DoWhile"] = BLOCK_TYPE_COUNTS["DoWhile"] + 1

    return ongoingCount

def traverseSwitch(caseBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS):

    # print(caseBlockList, end="\n\n")
    for block in caseBlock:
        if(block.get("_nodetype") == "If"):
            ongoingCount = ongoingCount + 1
            if(block.get("iftrue") != None):
                NESTING_COORDS.append(block.get("coord"))
                ongoingCount = traverseIfTrue(block.get("iftrue").get("block_items"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
                BLOCK_TYPE_COUNTS["If"] = BLOCK_TYPE_COUNTS["If"] + 1
            
            if(block.get("iffalse") != None):
                ongoingCount = traverseIfFalse(block.get("iffalse"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "Switch"):
            # print("nested switch found\n")
            ongoingCount = ongoingCount + 1
            BLOCK_TYPE_COUNTS["Switch"] = BLOCK_TYPE_COUNTS["Switch"] + 1
            NESTING_COORDS.append(block.get("coord"))
            for embeddedCaseBlock in block.get("stmt").get("block_items"):
                caseBlockContents = embeddedCaseBlock.get("stmts")
                ongoingCount = traverseSwitch(caseBlockContents, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "For"):
            ongoingCount = ongoingCount + 1
            # print("Outer for found!\n")   
            forBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseFor(forBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["For"] = BLOCK_TYPE_COUNTS["For"] + 1

        elif(block.get("_nodetype") == "While"):
            ongoingCount = ongoingCount + 1
            # print("embedded While found!\n")
            whileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseWhile(whileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["While"] = BLOCK_TYPE_COUNTS["While"] + 1

        elif(block.get("_nodetype") == "DoWhile"):
            ongoingCount = ongoingCount + 1
            # print("inner DoWhile found!\n")
            doWhileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseDoWhile(doWhileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)    
            BLOCK_TYPE_COUNTS["DoWhile"] = BLOCK_TYPE_COUNTS["DoWhile"] + 1

    return ongoingCount

def traverseFor(forBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS):
    # print(forBlock, end="\n\n\n")    

    # BLOCK_TYPE_COUNTS["For"] = BLOCK_TYPE_COUNTS["For"] + 1

    if(forBlock.get("_nodetype") == "Compound"):
        blockList = forBlock.get("block_items")
    else:
        blockList = [forBlock]
    
    if(blockList == None):
        return ongoingCount

    # for block in forBlock.get("block_items"):
    for block in blockList:
        if(block.get("_nodetype") == "If"):
            # print("if found inside a for")
            ongoingCount = ongoingCount + 1
            if(block.get("iftrue") != None):
                NESTING_COORDS.append(block.get("coord"))
                ongoingCount = traverseIfTrue(block.get("iftrue").get("block_items"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
                BLOCK_TYPE_COUNTS["If"] = BLOCK_TYPE_COUNTS["If"] + 1
            
            if(block.get("iffalse") != None):                
                ongoingCount = traverseIfFalse(block.get("iffalse"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)


        elif(block.get("_nodetype") == "Switch"):
            # print("nested switch found\n")
            ongoingCount = ongoingCount + 1
            BLOCK_TYPE_COUNTS["Switch"] = BLOCK_TYPE_COUNTS["Switch"] + 1
            NESTING_COORDS.append(block.get("coord"))
            for embeddedCaseBlock in block.get("stmt").get("block_items"):
                caseBlockContents = embeddedCaseBlock.get("stmts")
                ongoingCount = traverseSwitch(caseBlockContents, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "For"):
            ongoingCount = ongoingCount + 1
            # print("Outer for found!\n")   
            forBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseFor(forBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)    
            BLOCK_TYPE_COUNTS["For"] = BLOCK_TYPE_COUNTS["For"] + 1

        elif(block.get("_nodetype") == "While"):
            ongoingCount = ongoingCount + 1
            # print("embedded While found!\n")
            whileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseWhile(whileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["While"] = BLOCK_TYPE_COUNTS["While"] + 1

        elif(block.get("_nodetype") == "DoWhile"):
            ongoingCount = ongoingCount + 1
            # print("inner DoWhile found!\n")
            doWhileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseDoWhile(doWhileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["DoWhile"] = BLOCK_TYPE_COUNTS["DoWhile"] + 1

    return ongoingCount     


def traverseWhile(whileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS):
    # print("WhileBlock")   
    # BLOCK_TYPE_COUNTS["While"] = BLOCK_TYPE_COUNTS["While"] + 1

    if(whileBlock.get("_nodetype") == "Compound"):
        blockList = whileBlock.get("block_items")
    else:
        blockList = [whileBlock]

    if(blockList == None):
        return ongoingCount    

    for block in blockList:
        if(block.get("_nodetype") == "If"):
            # print("if found inside a while")
            ongoingCount = ongoingCount + 1
            if(block.get("iftrue") != None):
                NESTING_COORDS.append(block.get("coord"))
                ongoingCount = traverseIfTrue(block.get("iftrue").get("block_items"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
                BLOCK_TYPE_COUNTS["If"] = BLOCK_TYPE_COUNTS["If"] + 1
            
            if(block.get("iffalse") != None):
                ongoingCount = traverseIfFalse(block.get("iffalse"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "Switch"):
            # print("nested switch found\n")
            ongoingCount = ongoingCount + 1
            NESTING_COORDS.append(block.get("coord"))
            BLOCK_TYPE_COUNTS["Switch"] = BLOCK_TYPE_COUNTS["Switch"] + 1
            for embeddedCaseBlock in block.get("stmt").get("block_items"):
                caseBlockContents = embeddedCaseBlock.get("stmts")
                ongoingCount = traverseSwitch(caseBlockContents, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "For"):
            ongoingCount = ongoingCount + 1
            # print("embedded for found!\n")   
            NESTING_COORDS.append(block.get("coord"))
            forBlock = block.get("stmt")
            ongoingCount = traverseFor(forBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["For"] = BLOCK_TYPE_COUNTS["For"] + 1

        elif(block.get("_nodetype") == "While"):
            ongoingCount = ongoingCount + 1
            # print("embedded While found!\n")
            whileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseWhile(whileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["While"] = BLOCK_TYPE_COUNTS["While"] + 1

        elif(block.get("_nodetype") == "DoWhile"):
            ongoingCount = ongoingCount + 1
            # print("inner DoWhile found!\n")
            doWhileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseDoWhile(doWhileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["DoWhile"] = BLOCK_TYPE_COUNTS["DoWhile"] + 1
                            
    return ongoingCount

def traverseDoWhile(doWhileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS):
    # print(doWhileBlock, end="\n\n\n") 

    if(doWhileBlock.get("_nodetype") == "Compound"):
        blockList = doWhileBlock.get("block_items")
    else:
        blockList = [doWhileBlock]

    if(blockList == None):
        return ongoingCount

    for block in blockList:
        if(block.get("_nodetype") == "If"):
            # print("if found inside a dowhile")
            ongoingCount = ongoingCount + 1
            if(block.get("iftrue") != None):
                NESTING_COORDS.append(block.get("coord"))
                ongoingCount = traverseIfTrue(block.get("iftrue").get("block_items"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
                BLOCK_TYPE_COUNTS["If"] = BLOCK_TYPE_COUNTS["If"] + 1
            
            if(block.get("iffalse") != None):
                ongoingCount = traverseIfFalse(block.get("iffalse"), ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "Switch"):
            # print("nested switch found in do while\n")
            ongoingCount = ongoingCount + 1
            NESTING_COORDS.append(block.get("coord"))
            BLOCK_TYPE_COUNTS["Switch"] = BLOCK_TYPE_COUNTS["Switch"] + 1
            for embeddedCaseBlock in block.get("stmt").get("block_items"):
                caseBlockContents = embeddedCaseBlock.get("stmts")
                ongoingCount = traverseSwitch(caseBlockContents, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "For"):
            ongoingCount = ongoingCount + 1
            # print("embedded for found inside do while!\n")   
            forBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseFor(forBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)    
            BLOCK_TYPE_COUNTS["For"] = BLOCK_TYPE_COUNTS["For"] + 1

        elif(block.get("_nodetype") == "While"):
            ongoingCount = ongoingCount + 1
            # print("embedded While found inside a do while!\n")
            whileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseWhile(whileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["While"] = BLOCK_TYPE_COUNTS["While"] + 1

        elif(block.get("_nodetype") == "DoWhile"):
            ongoingCount = ongoingCount + 1
            # print("inner DoWhile found!\n")
            doWhileBlock = block.get("stmt")
            NESTING_COORDS.append(block.get("coord"))
            ongoingCount = traverseDoWhile(doWhileBlock, ongoingCount, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            BLOCK_TYPE_COUNTS["DoWhile"] = BLOCK_TYPE_COUNTS["DoWhile"] + 1

    return ongoingCount

def gather_metric(function_node):
    """ mandatory function for all gatherers """    
    nodes_in_function = function_node.get("body").get("block_items")
    block_nodes = list(
        filter(
            lambda block: (block.get("_nodetype") in COMPOUND_BLOCK_NODE_TYPES),
            nodes_in_function,
        )
    )

    count_of_nested_blocks = 0

    BLOCK_TYPE_COUNTS = {
        "For": 0,
        "If": 0,
        "While": 0,
        "DoWhile": 0,
        "Switch": 0
    }

    NESTING_COORDS = []

    # print(function_node.get("decl").get("name"))

    for block in BLOCK_TYPE_COUNTS:         #Initializing the counts of the nested block types
        # print(block)
        BLOCK_TYPE_COUNTS[block] = 0
    
    for block in block_nodes:
        if(block.get("_nodetype") == "If"):
            # print("Outer If found")
            if(block.get("iftrue") != None):
                # print(block.get("iftrue"), end="\n\n")
                count_of_nested_blocks = traverseIfTrue(block.get("iftrue").get("block_items"), count_of_nested_blocks, BLOCK_TYPE_COUNTS, NESTING_COORDS)
            
            if(block.get("iffalse") != None):
                # print(block.get("iffalse"), end="\n\n")
                count_of_nested_blocks = traverseIfFalse(block.get("iffalse"), count_of_nested_blocks, BLOCK_TYPE_COUNTS, NESTING_COORDS)
                

        elif(block.get("_nodetype") == "Switch"):
        #     print("Outer Switch found!\n") 

            for caseBlock in block.get("stmt").get("block_items"):
                caseBlockComponents = caseBlock.get("stmts")
                count_of_nested_blocks = traverseSwitch(caseBlockComponents, count_of_nested_blocks, BLOCK_TYPE_COUNTS, NESTING_COORDS) 

        elif(block.get("_nodetype") == "For"):
            # print("Outer for found!\n")

            forBlock = block.get("stmt")
            count_of_nested_blocks = traverseFor(forBlock, count_of_nested_blocks, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "While"):
            # print("Outer While found!\n")

            whileBlock = block.get("stmt")
            count_of_nested_blocks = traverseWhile(whileBlock, count_of_nested_blocks, BLOCK_TYPE_COUNTS, NESTING_COORDS)

        elif(block.get("_nodetype") == "DoWhile"):
            # print("Outer DoWhile found!\n")
            
            doWhileBlock = block.get("stmt")
            count_of_nested_blocks = traverseDoWhile(doWhileBlock, count_of_nested_blocks, BLOCK_TYPE_COUNTS, NESTING_COORDS)

    # print(count_of_nested_blocks)

    # block_type_coord_pairs = [
    #     create_block_type_coord_pair(block) for block in block_nodes
    # ]

    return (
        METRIC_NAME,
        {
            "TotalNestedBlocksInFunction": count_of_nested_blocks,
            "BlockTypeCounts": BLOCK_TYPE_COUNTS,
            "NestedBlocksStartCoordinates": NESTING_COORDS,
        },
    )


def create_block_type_coord_pair(block):
    """ creates a block type coordinate pair object and returns it """
    return {"type": block.get("_nodetype"), "location": block.get("coord")}
