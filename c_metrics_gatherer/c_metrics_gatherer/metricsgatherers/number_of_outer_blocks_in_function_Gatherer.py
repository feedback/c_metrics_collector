"""NumberOfBlocksInFunctionGatherer returns the number of complex outer blocks in the function"""

METRIC_NAME = "NumberOfOuterBlocksInFunction"
COMPOUND_BLOCK_NODE_TYPES = ["For", "If", "While", "DoWhile", "Switch"]


def gather_metric(function_node):
    """ mandatory function for all gatherers """
    nodes_in_function = function_node.get("body").get("block_items")
    block_nodes = list(
        filter(
            lambda block: (block.get("_nodetype") in COMPOUND_BLOCK_NODE_TYPES),
            nodes_in_function,
        )
    )
    number_of_blocks = len(block_nodes)

    block_type_coord_pairs = [
        create_block_type_coord_pair(block) for block in block_nodes
    ]

    return (
        METRIC_NAME,
        {
            "NumberOfOuterBlocksInFunction": number_of_blocks,
            "blocks": block_type_coord_pairs,
        },
    )


def create_block_type_coord_pair(block):
    """ creates a block type coordinate pair object and returns it """
    return {"type": block.get("_nodetype"), "location": block.get("coord")}
