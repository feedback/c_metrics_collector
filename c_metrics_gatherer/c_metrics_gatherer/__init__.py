"""Top-level package for c_metrics_gatherer."""

__author__ = """Nathan Laundry"""
__email__ = 'nlaundry@uoguelph.ca'
__version__ = '0.1.0'
