"""
module that handles programmatically importing all gatherers
and accumulating the data
"""

import glob
import importlib
import os

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
# all gatherer files must end in Gatherer
METRICS_GATHERER_FILE_NAME = "/metricsgatherers/*Gatherer.py"
# metrics_gatherers package
METRICS_GATHERHER_PACKAGE = "c_metrics_gatherer.metricsgatherers."

# Programmatically imports a list of metric gatherers from a subpackage


def import_metric_gatherers():
    """ imports all metric gatherers from metricgatherers subpkg """
    # Gatherers must be terminate their name with Gatherer
    gatherer_files = [
        os.path.basename(x) for x in glob.glob(CURRENT_DIR + METRICS_GATHERER_FILE_NAME)
    ]
    gatherer_modules = [x.replace(".py", "") for x in gatherer_files]
    modules = []
    for gatherer_module in gatherer_modules:
        modules.append(
            importlib.import_module(METRICS_GATHERHER_PACKAGE + gatherer_module)
        )
    return modules


def gather_all_metrics(metrics_gatherers, function_node_list):
    """
    returns a list of objects that contain metrics specific to a function
    Each metrics object contains a set of key value pairs, metric_name + numeric value
      or a key of metric name + an object containing various data
    Each metrics object also contains coords by filename, and line
    """
    metrics = []
    for function_node in function_node_list:
        function_metrics = {
            "functionName": function_node.get("decl").get("name"),
            "coord": function_node.get("coord"),
        }
        for gatherer in metrics_gatherers:
            metric_name, metric_data = gatherer.gather_metric(function_node)
            function_metrics[metric_name] = metric_data
        metrics.append(function_metrics)
    return metrics


def filter_for_functions(ast_dict):
    """filters out all top level non function nodes from the dictionary"""
    nodes = ast_dict.get("ext")
    return list(filter(lambda x: (x.get("_nodetype") == "FuncDef"), nodes))


# Receives a list of dictionaries
def generate_metrics_json(ast_dicts):
    """
    gathers all metrics after formatting ast_dicts for analysis
    receives a list of dictionaries
    """
    # creates a list of lists of function nodes. The sublists are specific to the file
    list_of_function_node_lists = [filter_for_functions(x) for x in ast_dicts]
    # creates a flat list of all function_nodes so they aren't separated by file
    function_node_list = [
        item for sublist in list_of_function_node_lists for item in sublist
    ]

    gatherer_modules = import_metric_gatherers()
    return gather_all_metrics(gatherer_modules, function_node_list)
