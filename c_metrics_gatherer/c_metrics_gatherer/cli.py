"""Console script for c_metrics_gatherer."""
import argparse
import sys

from .c_metrics_gatherer import generate_metrics


def parse_args():
    """ parses commandline arguments and returns the parsed arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s",
        "--submission",
        help="Specify the directory that holds the student submissions.",
    )
    parser.add_argument(
        "-o", "--output", help="specifies the name and location for an output file"
    )
    parser.add_argument(
        "-c", "--c_headers", help="specifies the location of the fake c header files"
    )
    return parser.parse_args()


def main():
    """Console script for c_metrics_gatherer."""
    generate_metrics(parse_args())
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
