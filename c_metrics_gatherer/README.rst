==================
c_metrics_gatherer
==================


.. image:: https://img.shields.io/pypi/v/c_metrics_gatherer.svg
        :target: https://pypi.python.org/pypi/c_metrics_gatherer

.. image:: https://img.shields.io/travis/nlaundry/c_metrics_gatherer.svg
        :target: https://travis-ci.com/nlaundry/c_metrics_gatherer

.. image:: https://readthedocs.org/projects/c-metrics-gatherer/badge/?version=latest
        :target: https://c-metrics-gatherer.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Gathers metrics from source C files by generating an AST from pycparser


* Free software: MIT license
* Documentation: https://c-metrics-gatherer.readthedocs.io.


Features
--------

* Given a submission of C code, recursively traverses directory structure
  to genereate an AST for all source files
* Produces metrics for all ASTs in JSON format
* Current metrics include:
  * Counting the number of compound blocks in a function
  * Counting and listing parameters of all functions 
* Can be run with a command line interface c_metrics_gatherer

* Example run:
.. code:: bash
  c_metrics_gatherer -s ./testSubmission -o ./output/metrics.json


Code Structure and Adding new Metrics
-------------------------------------

* Program execution starts at cli.py and directs arguments to c_metrics_gatherer.py
* From c_metrics_gatherer.py the program finds all C source files from the submission
  and genereates ASTs given the code in generateAST.py
* Once a list of ASTS represeneted as dictionaries are produced
  runGatherers.py contains code that manages running the gatherers in the metricsgatherers subpkg
* each metric gatherer is programmatically imported as long as it follows the following conditions:
    * must be inside the metricsgatherers subpkg ( directory )
    * must end in Gatherer.py, example newmetricGatherer.py
    * must contain a method called gather_metric that receives some portion of the ast_dict
    * must return the name of the metric and an object containing data:
      return "metricName" , { "data": data }
    * The metric is then appended to the dictionary that will be printed as json to a file

Output Data Structure
---------------------

* As of now, all metrics are specific to functions so we group metrics by the function

.. code:: json
    [
        {
            "functionName": "someFunction",
            "coord": "./testSubmission/src/seconary.c:3:5",
            "NumberOfBlocksInFunction": {
                "NumberOfBlocksInFunction": 0,
                "blocks": []
            },
            "ParameterListData": {
                "numParams": 1,
                "paramList": [
                    {
                        "name": "myParam",
                        "type": "int",
                        "isPtr": false
                    }
                ]
            }
        },
        .
        .
        .
    ]

* so the output is a list of objects grouped by the function, including a list of metrics objects


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

This package contains a version of pycparser and also uses it as a requisite library.
pycparser is used to generate ASTs to perform analysis on and is integral to the project.
generateAST.py is also largely an example file from the pycparser repo

.. _Pycparser: https://pypi.org/project/pycparser/
.. _`eliben/pycparser`: https://github.com/eliben/pycparser
