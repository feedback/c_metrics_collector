#!/usr/bin/env python

"""Tests for `c_metrics_gatherer` package."""


import unittest
import unittest.mock
import os
import io

from c_metrics_gatherer import c_metrics_gatherer

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))


class Namespace:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class TestC_metrics_gatherer(unittest.TestCase):
    """Tests for `c_metrics_gatherer` package."""

    test_files_dir = CURRENT_DIR + "/testSubmissions"

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_valid_files(self):
        """Test valid C submission"""
        exit_status = os.system(
            "c_metrics_gatherer -s"
            + self.test_files_dir
            + "/valid_with_headers -o metrics.json"
        )
        self.assertEqual(exit_status, 0)
        # if generate_metrics runs successfully an output file named
        # metrics.json will be created
        self.assertTrue(os.path.isfile("metrics.json"))
        # self.assertEqual(mock_stdout.getvalue(), "")

    @unittest.mock.patch("sys.stdout", new_callable=io.StringIO)
    def test_no_src_files(self, mock_stdout):
        """Test no source files provided"""
        args = Namespace(
            submission=self.test_files_dir + "/no_c_files", output="metrics.json"
        )
        c_metrics_gatherer.generate_metrics(args)
        self.assertEqual(mock_stdout.getvalue(), "No valid source files given\n")

    @unittest.mock.patch("sys.stdout", new_callable=io.StringIO)
    def test_only_headers_provided(self, mock_stdout):
        """Test no source files provided"""
        args = Namespace(
            submission=self.test_files_dir + "/only_header_files", output="metrics.json"
        )
        c_metrics_gatherer.generate_metrics(args)
        self.assertEqual(mock_stdout.getvalue(), "no measurable metrics were produced\n")

