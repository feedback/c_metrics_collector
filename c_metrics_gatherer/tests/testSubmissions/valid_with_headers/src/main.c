#include <stdio.h>

int main(){
    int x = 5;

    if ( x == 5 ) {
        printf( "x is equal to 5" );
    } else if ( x != 5 ) {
        printf( "x is not equal to 5" );
    }

    for ( int i = 0; i < x; i ++ ) {
        printf( "I'm printing things" );
    }

    while ( x == 5 ) {
        x ++;
    }

    do {
        x ++;
    } while ( x == 7 );

    switch( x ) {
        case 1:
            printf( "some stuff" );
            break;
        case 5:
            printf( "some other stuff" );
            break;
        default:
            printf( "wow that's crazy dude" );
            break;
    }
}
