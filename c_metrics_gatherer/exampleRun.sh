#outputs to console that we're running the code smell analyzer
echo "Running C Code smell test"

#Create src directory
mkdir -p src

#move all files into src
mv * src/

#creates a new directory
mkdir -p .feedback_artifacts

#Uses the gitlab api to fetch a single file from a repository. The header is a key that unlocks the private repo for us
#Recall that this is specifically references the AST-Redo branch. WILL NEED TO CHANGE WHEN BRANCHES ARE UDPATED
curl --header "IFS:3av-hVpaEkRk1Uzy2jgf" "https://gitlab.socs.uoguelph.ca/api/v4/projects/feedback%2Fc_metrics_collector/repository/files/c_metrics_gatherer%2Fdist%2Ffake_libc_include.zip/raw?ref=AST-Redo" --output fake_c_headers.zip

#unzip the zip into a dir called headers, -d names that dir
unzip -d headers fake_libc_include.zip

#change directories so results end up in this dir then run the test
cd .feedback_artifacts

#generate metrics
c_metrics_gatherer -s ../src/ -c ../headers/ -o metrics.json